Pod::Spec.new do |s|
  s.name         = "ICACloud"
  s.version      = "1.0"
  s.summary      = "ICACloud"
  s.description  = "ICACloud"
  s.homepage     = "https://github.com/drewmccormack"
  s.license      = "MIT"
  s.authors      = { "Drew McCormack" => "drewmccormack@mac.com" }
  s.ios.deployment_target = "8.0"
  s.tvos.deployment_target = "9.0"
  s.osx.deployment_target = "10.7"
  s.watchos.deployment_target = "2.0"
  s.source = { :git => "https://github.com/drewmccormack/icloudaccess.git",
               :branch=>'master' }
  s.source_files  = [
    "ICACloud.{h,m}",
  ]
  s.header_mappings_dir = "."
  s.public_header_files = [
    "ICACloud.h",
  ]
  s.requires_arc = true
end
